Activity
  
  /*1) Find users with letter 's' in their first name or 'd' in their last name.
		Show only the firstName and lastName fields and hide the _id field.*/

   db.users.find( { $or: [ {firstName: { $regex: 's', $options: '$i' } }, {lastName: { $regex: 'd', $options: '$i' } } ] },
      { 
        firstName: 1, 
        lastName: 1,
        _id: 0
      }
    ).pretty();
    
  /*2) Find users who are from the HR department 
  and their age is greater then or equal to 70.*/

   db.users.find({ $and:
    [{
      department: "HR department"
    },
    { 
      age: { $gte : 70 }
    }]
    });
  
/*  3) Find users with the letter 'e' in their first name 
  and has an age of less than or equal to 30.*/

   db.users.find({ $and:
    [{
      firstName: { $regex: "e" , $options: "$i"}
    },
    { 
      age: { $lte : 30 }
    }]
    });



===== Users Collection =====
{
    "_id" : ObjectId("624edb1d09e0668c07dd1036"),
    "firstName" : "John",
    "lastName" : "Smith"
}

/* 2 */
{
    "_id" : ObjectId("624edd1509e0668c07dd1037"),
    "firstName" : "Joe",
    "lastName" : "Doe"
}

/* 3 */
{
    "_id" : ObjectId("624edd1509e0668c07dd1038"),
    "firstName" : "Jane",
    "lastName" : "Doe"
}

{
    "_id" : ObjectId("624ee53c09e0668c07dd103b"),
    "firstName" : "Neil",
    "lastName" : "Armstrong",
    "age" : 82.0,
    "contact" : {
        "email" : "neil.armstrong@mail.com",
        "phone" : "09987654321"
    },
    "department" : "HR department"
}

/* 6 */
{
    "_id" : ObjectId("624ee53c09e0668c07dd103a"),
    "firstName" : "Neil",
    "lastName" : "Diamond",
    "age" : 81.0,
    "contact" : {
        "email" : "neil.diamond@mail.com",
        "phone" : "09123456789"
    },
    "department" : "HR department"
}

{
    "_id" : ObjectId("624ee046b85fda38d649a836"),
    "firstName" : "Denzel",
    "lastName" : "Flores",
    "age" : 22.0,
    "contact information" : {
        "email" : "denzel.flores@mail.com",
        "phone" : 2345326.0
    },
    "department" : "HR department"

{
    "_id" : ObjectId("625424b6d4df32c789db5646"),
    "firstName" : "Stephen",
    "lastName" : "Hawking",
    "age" : 76.0,
    "contact" : {
        "email" : "stephen.hawking@mail.com",
        "phone" : 9987654321.0
    },
    "department" : "HR department"
}


============================

